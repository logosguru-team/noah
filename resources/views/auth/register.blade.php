@extends('layouts.auth')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 ">
    <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
        <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">Register</h3>
                    </div>
                </div>
            </div>
            <form class="m-form m-form--state" method="POST" action="{{ url('/register') }}">
                {{ csrf_field() }}
                <div class="m-portlet__body">
                    @if(session()->has('message'))
                    <div class="alert alert-info" role="alert">
					{{ session()->get('message') }}
                    </div>
                    @endif
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group {{ $errors->has('first_name') ? ' has-danger' : '' }}">
                            <label for="first_name">First Name:</label>
                            <input type="text" id="first_name" name="first_name" class="form-control m-input" value="{{ old('first_name') }}" placeholder="Enter first name" autofocus>
                            @if ($errors->has('first_name'))
                                <div class="form-control-feedback">{{ $errors->first('first_name') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group {{ $errors->has('last_name') ? ' has-danger' : '' }}">
                            <label for="last_name">Last Name:</label>
                            <input type="text" id="last_name" name="last_name" class="form-control m-input" value="{{ old('last_name') }}" placeholder="Enter last name">
                            @if ($errors->has('last_name'))
                                <div class="form-control-feedback">{{ $errors->first('last_name') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group {{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label for="email">Email:</label>
                            <input type="text" id="email" name="email" class="form-control m-input" value="{{ old('email') }}" placeholder="Enter email">
                            @if ($errors->has('email'))
                                <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                            @endif
                        </div>
                        <div class="form-group m-form__group row {{ $errors->has('password') ? ' has-danger' : '' }}">
                            <div class="col-lg-6">
                                <label for="password">Password:</label>
                                <input type="password" id="password" name="password" class="form-control m-input">
                                @if ($errors->has('password'))
                                    <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                            <div class="col-lg-6">
                                <label for="password-confirm">Confirm Password:</label>
                                <input type="password" id="password-confirm" name="password_confirmation" class="form-control m-input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot">
                    <input type="hidden" name="type" value="admin" />
                    <button type="submit" class="btn btn-primary">Submit</div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
