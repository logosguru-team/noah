<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8" />
		<title>{{ config('app.name', 'Noah') }}</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
		<!--end::Web font -->
        <link href="/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="/assets/front/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="/assets/front/img/logo/favicon.ico" />
	</head>
	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--wide m-header--fixed m-header--fixed-mobile m-footer--push m-aside--offcanvas-default m-header--minimize-off" data-gr-c-s-loaded="true">
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <header id="m_header" class="m-grid__item m-header " m-minimize="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200">
                <div class="m-header__top">
                    <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                        <div class="m-stack m-stack--ver m-stack--desktop">		
                        <!-- begin::Brand -->
                            <div class="m-stack__item m-brand">
                                <div class="m-stack m-stack--ver m-stack--general m-stack--inline">
                                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                        <a href="?page=index&amp;demo=demo5" class="m-brand__logo-wrapper">
                                            <img alt="" src="/assets/front/img/logo/logo.png">
                                        </a>
                                    </div>
                                    <div class="m-stack__item m-stack__item--middle m-brand__tools">
                                    </div>
                                </div>
                            </div>
                        <!-- end::Brand -->		
                        </div>
                    </div>
                </div>
                <div class="m-header__bottom">
                    <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                        <div class="m-stack m-stack--ver m-stack--desktop">
                            <!-- begin::Horizontal Menu -->
                            <div class="m-stack__item m-stack__item--middle m-stack__item--fluid">
                                <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-dark m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-light m-aside-header-menu-mobile--submenu-skin-light ">                            
                                    <ul class="m-menu__nav m-menu__nav--submenu-arrow">
                                        <li class="m-menu__item m-menu__item--active" aria-haspopup="true">
                                            <a href="/" class="m-menu__link "><span class="m-menu__item-here"></span><span class="m-menu__link-text">Home</span></a>
                                        </li>
                                        <li class="m-menu__item m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
                                            <a href="javascript:;" class="m-menu__link m-menu__toggle" title="Non functional dummy link">
                                                <span class="m-menu__item-here"></span><span class="m-menu__link-text">Reports</span>
                                                <i class="m-menu__hor-arrow la la-angle-down"></i>
                                                <i class="m-menu__ver-arrow la la-angle-right"></i>
                                            </a>
                                            <div class="m-menu__submenu  m-menu__submenu--fixed m-menu__submenu--left" style="width:600px">
                                                <span class="m-menu__arrow m-menu__arrow--adjust" style="left: 56.5px;"></span>
                                                <div class="m-menu__subnav">
                                                    <ul class="m-menu__content">
                                                        <li class="m-menu__item">
                                                            <h3 class="m-menu__heading m-menu__toggle"><span class="m-menu__link-text">Finance Reports</span><i class="m-menu__ver-arrow la la-angle-right"></i></h3>
                                                            <ul class="m-menu__inner">
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-icon flaticon-map"></i><span class="m-menu__link-text">Annual Reports</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-icon flaticon-user"></i><span class="m-menu__link-text">HR Reports</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-icon flaticon-clipboard"></i><span class="m-menu__link-text">IPO Reports</span></a>
                                                                </li>
                                                                <li class="m-menu__item" m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-icon flaticon-graphic-1"></i><span class="m-menu__link-text">Finance Margins</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-icon flaticon-graphic-2"></i><span class="m-menu__link-text">Revenue Reports</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li class="m-menu__item">
                                                            <h3 class="m-menu__heading m-menu__toggle"><span class="m-menu__link-text">Project Reports</span><i class="m-menu__ver-arrow la la-angle-right"></i></h3>
                                                            <ul class="m-menu__inner">
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Coca Cola CRM</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Delta Airlines Booking Site</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Malibu Accounting</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Vineseed Website Rewamp</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="/" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Zircon Mobile App</span></a>
                                                                </li>
                                                                <li class="m-menu__item " m-menu-link-redirect="1" aria-haspopup="true">
                                                                    <a href="?page=inner&amp;demo=demo5" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--line"><span></span></i><span class="m-menu__link-text">Mercury CMS</span></a>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- end:Horizontal Menu -->
                        </div>
                    </div>
                </div>
            </header>
    		<!-- begin:: Page -->
            <div class="m-grid__item m-grid__item--fluid  m-grid m-grid--ver-desktop m-grid--desktop 	m-container m-container--responsive m-container--xxl m-page__container m-body">
                <div class="m-grid__item m-grid__item--fluid m-wrapper">
                    <div class="m-content">
                        @yield('content')
                    </div>
                </div>
            </div>
		    <!-- end:: Page -->
            <footer class="m-grid__item m-footer ">
                <div class="m-container m-container--responsive m-container--xxl m-container--full-height m-page__container">
                    <div class="m-footer__wrapper">
                        <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                            <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                                <span class="m-footer__copyright">
                                    2018 © Noah Inc.
                                </span>
                            </div>
                            <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                                <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                                    <li class="m-nav__item">
                                        <a href="#" class="m-nav__link">
                                            <span class="m-nav__link-text">About</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="#" class="m-nav__link">
                                            <span class="m-nav__link-text">Privacy</span>
                                        </a>
                                    </li>
                                    <li class="m-nav__item">
                                        <a href="#" class="m-nav__link">
                                            <span class="m-nav__link-text">T&amp;C</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>	
                        </div>
                    </div>
                </div>
            </footer>        
        </div>    
		<!--begin::Global Theme Bundle -->
		<script src="/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="/assets/front/js/scripts.bundle.js" type="text/javascript"></script>
        <script src="/assets/front/js/dashboard.js" type="text/javascript"></script>
		<!--end::Global Theme Bundle -->
	</body>
	<!-- end::Body -->
</html>
