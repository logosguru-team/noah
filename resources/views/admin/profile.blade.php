@extends('layouts.admin')

@section('content')

    <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--full-height">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">Update Profile</h3>
                </div>
            </div>
        </div>
        <form class="m-form m-form--state" method="POST" action="{{ route('admin.profile.update', $user->id) }}">
            @method('PATCH')
            {{ csrf_field() }}
            <div class="m-portlet__body">
                @if(session()->has('message'))
                <div class="alert alert-{{ session()->get('message_type') }}" role="alert">
                    {{ session()->get('message') }}
                </div>
                @endif
                <div class="m-form__section m-form__section--first">
                    <div class="form-group m-form__group {{ $errors->has('first_name') ? ' has-danger' : '' }}">
                        <label for="first_name">First Name:</label>
                        <input type="text" id="first_name" name="first_name" class="form-control m-input" value="{{ $user->first_name }}" placeholder="Enter first name" autofocus>
                        @if ($errors->has('first_name'))
                            <div class="form-control-feedback">{{ $errors->first('first_name') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group {{ $errors->has('last_name') ? ' has-danger' : '' }}">
                        <label for="last_name">Last Name:</label>
                        <input type="text" id="last_name" name="last_name" class="form-control m-input" value="{{ $user->last_name }}" placeholder="Enter last name">
                        @if ($errors->has('last_name'))
                            <div class="form-control-feedback">{{ $errors->first('last_name') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group {{ $errors->has('email') ? ' has-danger' : '' }}">
                        <label for="email">Email:</label>
                        <input type="text" id="email" name="email" class="form-control m-input" value="{{ $user->email }}" placeholder="Enter email">
                        @if ($errors->has('email'))
                            <div class="form-control-feedback">{{ $errors->first('email') }}</div>
                        @endif
                    </div>
                    <div class="form-group m-form__group row {{ $errors->has('password') ? ' has-danger' : '' }}">
                        <div class="col-lg-6">
                            <label for="password">Password:</label>
                            <input type="password" id="password" name="password" class="form-control m-input">
                            <span class="m-form__help">Leave blank if you don't want to change password.</span>
                            @if ($errors->has('password'))
                                <div class="form-control-feedback">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <label for="password-confirm">Confirm Password:</label>
                            <input type="password" id="password-confirm" name="password_confirmation" class="form-control m-input">
                        </div>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot">
                <input type="hidden" name="type" value="{{ $user->type }}" />
                <button type="submit" class="btn btn-primary">Update</div>
            </div>
        </form>
    </div>

@endsection
