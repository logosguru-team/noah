const mix = require('laravel-mix');

var adminTheme = 'default';
var frontTheme = 'demo2';

var adminJsPath = '../resources/themes/js/demo/'+adminTheme+'/';
var adminCssPath = '../resources/themes/sass/demo/'+adminTheme+'/';

var frontJsPath = '../resources/themes/js/demo/'+frontTheme+'/';
var frontCssPath = '../resources/themes/sass/demo/'+frontTheme+'/';

/* Admin */
mix.combine(['../resources/js/admin/dashboard.js'],'../public/assets/admin/js/dashboard.js')
    .combine([
        '../resources/themes/js/framework/base/util.js',
        '../resources/themes/js/framework/base/app.js',
        '../resources/themes/js/framework/components/general/datatable/datatable.js',
        '../resources/themes/js/framework/components/plugins/misc/mdatatable.init.js',
        '../resources/themes/js/framework/components/general/**/*.js',
        '../resources/themes/js/snippets/base/**/*.js',
        adminJsPath + '/**/*.js',
    ],'../public/assets/admin/js/scripts.bundle.js')
    .sass(adminCssPath + 'style.scss', '../public/assets/admin/css/style.bundle.css')
    .sass('../resources/sass/admin/app.scss', '../public/assets/admin/css/app.css');

/* Front */
mix.combine(['../resources/js/front/dashboard.js'],'../public/assets/front/js/dashboard.js')
    .combine([
        '../resources/themes/js/framework/base/util.js',
        '../resources/themes/js/framework/base/app.js',
        '../resources/themes/js/framework/components/general/datatable/datatable.js',
        '../resources/themes/js/framework/components/plugins/misc/mdatatable.init.js',
        '../resources/themes/js/framework/components/general/**/*.js',
        '../resources/themes/js/snippets/base/**/*.js',
        frontJsPath + '/base/**/*.js',
    ],'../public/assets/front/js/scripts.bundle.js')
    .sass(frontCssPath + 'style.scss', '../public/assets/front/css/style.bundle.css');