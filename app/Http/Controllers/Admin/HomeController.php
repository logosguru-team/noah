<?php

namespace Noah\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Noah\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('admin.index');
    }
}
