<?php

namespace Noah\Http\Controllers\Admin;

use Noah\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Noah\Http\Controllers\Controller;

class UserController extends Controller
{
    //
    public function profile()
    {
        $user = Auth::user();
        return view('admin.profile')->with('user',$user);
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'email'         => 'required|string|email|max:255|unique:users,email,'.$id,
            'password'      => 'nullable|string|min:6|confirmed',
        ]);

        try {
            $user = User::find($id);

            $user->first_name = $request->get('first_name');
            $user->last_name = $request->get('last_name');
            $user->email = $request->get('email');
            if (array_has($validatedData, 'password'))
                $user->password = bcrypt(array_get($validatedData, 'password'));

            $user->save();
        }
        catch (\Exception $exception) {
            logger()->error($exception);
            return redirect()->back()->with([
                'message' => 'Unable to update user profile.',
                'message_type' => 'danger'
            ]);
        }

        return redirect()->back()->with([
            'message' => 'User profile has been updated.',
            'message_type' => 'info'
        ]);
        
    }
}
