<?php

namespace Noah\Http\Controllers\Auth;

use Noah\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    public function redirectTo() 
    {
        $user = Auth::user();

        if ($user->isAdmin())
            return '/admin';
        else if ($user->isClient())
            return '/client';
        else
            return '/';
    } 

    protected function authenticated(Request $request)
    {
        $user = Auth::user();

        if ($user->status == 1) {
            // Authentication passed...
            return redirect()->intended($this->redirectTo());
        }
        else 
            return redirect()->back()->with([
                'message' => 'Please check your email and activate account.',
                'message_type' => 'danger'
            ]);
    }
    

}
