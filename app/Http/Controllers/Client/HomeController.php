<?php

namespace Noah\Http\Controllers\Client;

use Illuminate\Http\Request;
use Noah\Http\Controllers\Controller;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('client.index');
    }
}
