<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');

Route::group(['prefix' => 'client', 'middleware' => ['auth','client']], function () {
    Route::get('/', 'Client\HomeController@index')->name('client.home');
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth','admin']], function () {
    Route::get('/', 'Admin\HomeController@index')->name('admin.home');
    Route::get('profile', 'Admin\UserController@profile')->name('admin.profile');
    Route::patch('profile/{id}', 'Admin\UserController@update')->name('admin.profile.update');
});